﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Efcore_codeFirst.ViewModel;
using Microsoft.EntityFrameworkCore;

namespace Efcore_codeFirst.Data
{
    public class DeveloperDbContext : DbContext
    {
        public DeveloperDbContext(DbContextOptions<DeveloperDbContext> options) : base(options)
        {
        }
        public DbSet<Developer> Developer { get; set; }
        public DbSet<Country> Country { get; set; }


        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.ApplyConfiguration(new DeveloperEntityConfiguration());
        //}
    }
}
